# Создайте иерархию классов транспортных средств.
#  В общем классе опишите общие для всех транспортных средств поля,
#  в наследниках – специфичные для них. Создайте несколько экземпляров.
# Выведите информацию о каждом транспортном средстве.

# Самостоятельная деятельность учащегося
# Задание 1
# Создайте класс Editor,
# который содержит методы view_document и edit_document.
# Пусть метод edit_document выводит на экран информацию о том,
#  что редактирование документов недоступно для бесплатной версии.
# Создайте подкласс ProEditor, в котором данный метод будет переопределён.
#  Введите с клавиатуры лицензионный ключ и, если он корректный,
# создайте экземпляр класса ProEditor, иначе Editor.
# Вызовите методы просмотра и редактирования документов.


class Editor:
    def view_document(self):
        print('View document')

    def edit_document(self):
        print('Trial Edit document')


class ProEditor(Editor):
    def edit_document(self):
        print('Pro edit document')


def main():
    key = input('Enter key: ')
    if key == '123':
        print('Edit ok')
        editor = ProEditor()
    else:
        print('Incorect')
        editor = Editor()

    editor.view_document()
    editor.edit_document()


if __name__ == '__main__':
    main()
