# Задание 3
# Создайте иерархию классов с использованием множественного наследования.
#  Выведите на экран порядок разрешения методов для каждого из классов.
# Объясните, почему линеаризации данных классов выглядят именно так.

class Car:
    def drive(self):
        print('We go')


class Horse:
    def create_horse(self):
        print('Create Horse model')


class Trojan_Horse(Car, Horse):
    pass


class Control:
    pass


def main():
    th = Trojan_Horse()
    th.drive()
    th.create_horse()


if __name__ == '__main__':
    main()
